const Comment = ( { comment } ) => {


    return (
        <div className="comment">
            <hr />
            <p>{comment.message}</p>
            <hr />
        </div>
    )

}

export default Comment