import { render, screen } from "@testing-library/react"
import Comment from "../Comment"

describe("Comment", () => {

    test("Should render comment with message", () => {
        // Given
        const testMessage = {message: "Hej"}

        // When
        render(<Comment comment={testMessage} />)
        //screen.debug()
        const messageElement = screen.getByText(testMessage.message)

        // Then
        expect(messageElement).toHaveTextContent(testMessage.message)

    })

    test("Should render same snapshot", () => {
        // Given
        const testMessage = {message: "Hej"}

        // When
        const tree = render(<Comment comment={testMessage} />).asFragment()

        expect(tree).toMatchSnapshot()
    })

})