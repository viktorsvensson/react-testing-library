import { fireEvent, render, screen } from "@testing-library/react";
import CommentArea from "../CommentArea";


describe("CommentArea", () => {

    test("Should display an inputed comment in list", () => {
        // Given
        const testMessage = "testmessage";

        // When
        render(<CommentArea  />)
        const inputField = screen.getByPlaceholderText(/Add comment/i)
        fireEvent.change(inputField, { target: {value: testMessage}})

        const button = screen.getByRole("button", {name: "Add"})
        fireEvent.click(button)

        //Then
        const comment = screen.getByText(testMessage)
        expect(comment).toBeInTheDocument()

    })



})