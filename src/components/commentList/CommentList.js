import Comment from "../comment/Comment"

const CommentList = ({ comments }) => {

    return (
        comments.map((comment, index) => 
            <Comment key={index} comment={comment} />)
    )

}

export default CommentList