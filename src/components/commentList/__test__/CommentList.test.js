import { render, screen } from "@testing-library/react"
import CommentList from "../CommentList"


describe("CommentList", () => {

    test("Should display correct amount of comments", () => {
        // Givet
        const commentList = [
            {message: "testmessage-1"},
            {message: "testmessage-2"}
        ]

        // When
        render(<CommentList comments={commentList} />)
        const commentElementsList = screen.getAllByText(/testmessage-/i)
        expect(commentElementsList.length).toBe(2)
    })

})