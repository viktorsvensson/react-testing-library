import { useState } from "react"


const CommentInput = ({setComments}) => {

    const [text, setText] = useState("")

    const handleClick = () => {
        setComments(prev => [...prev, {message: text}])
    }

    return (
        <div>
            <h3>Add comment section</h3>
            <input
                placeholder="Add comment ..."
                value={text}
                onChange={e => setText(e.target.value)}
            />
            <button data-cy="addBtn" onClick={handleClick}>Add</button>
        </div>
    )
}

export default CommentInput;