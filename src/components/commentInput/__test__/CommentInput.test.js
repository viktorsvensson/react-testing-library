import { render, fireEvent, screen } from "@testing-library/react"
import CommentInput from "../CommentInput"


describe("CommentInput", () => {

    test("Should allow two way binding input", () => {
        // Given
        const testInput = "teststring"

        // When
        render(<CommentInput />)
        const inputField = screen.getByPlaceholderText(/Add comment/i)
        fireEvent.click(inputField)
        fireEvent.change(inputField, {target: {value: testInput}})

        // Then
        expect(inputField).toHaveValue(testInput)


    })

})