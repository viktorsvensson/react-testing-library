import './App.css';
import CommentArea from './components/commentArea/CommentArea';

function App() {
  return (
    <div className="App">
      <h1>Comments 3000</h1>
      <CommentArea />
    </div>
  );
}

export default App;
