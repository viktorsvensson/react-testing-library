/// <reference types="cypress" />

describe("Demo tests", () => {

    beforeEach(() => {
        cy.visit("/")

    })

    it("Should be able to visit our site", () => {

    })

    it("Should be able to visit our site", () => {

        cy.get('input[placeholder="Add comment ..."]').type("Hej")

        cy.get('[data-cy="addBtn"]').click()

        cy.get(".comment")
            .should('have.length', 2)
            .last()
            .should('have.text', "Hej")

    })

})